# Juggernaut

This theme is inspired by my favorite wine ([Juggernaut](https://juggernautwines.com/) - Hillside - Cabernet Sauvignon). It just so happens to objectively have the best label ever put on a bottle of wine and a very nice color scheme. 

![JugLogo](juglogo.png)

See, its pretty cool right? In their marketing/trade material they have the pantones listed for their labels and I thought it would make a great terminal theme.

![Patones](pantones.png)

Inspiration:
![Label](Juggernaut-HSC-B.png)

## Sample

![Screenshot](juggernaut.png)


I am in no way affiliated with Juggernaut. I'm merely a fan of the 🍷, label, colors and a nice terminal.
