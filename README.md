# iTerm2 Custom Colors

This is just a collection of iTerm2 Color Schemes I have created.

To set your iTerm2 Colors to one of these you can go to `iTerm2 > Preferences > Profiles > Colors > Color Presets > Import`

![Screenshot](importcolors.png)
